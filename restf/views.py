# from django.http import JsonResponse
# from rest_framework.parsers import JSONParser
# from rest_framework.response import Response
# from django.shortcuts import render
# from django.views.generic import CreateView
# from rest_framework.views import APIView
from rest_framework.response import Response

from restf.models import Profile

from rest_framework import viewsets, request
# Create your views here
from restf.serializers import ProfileReadSerializer, ProfileWriteSerializer


# class DeveloperViewSet(viewsets.ModelViewSet):
#     queryset = Developer.objects.all()
#     serializer_class = DeveloperSerializer

class ProfileView(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    # data = request.data
    serializer_class = ProfileReadSerializer

    def edit_serializer_class(self):
        if self.request.method == 'POST':
            self.serializer_class = ProfileWriteSerializer
        # return Response(self.serializer_class.data)`
        #
# class UsercreateView(CreateView):
#     model = Developer
#     fields = '__all__'
# template_name = 'restf/form.html'
# success_url = '/app/'
