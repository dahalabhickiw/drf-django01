from datetime import timedelta

from django.db import models
from django.contrib.auth.models import User


# Create your models here.


class Profile(models.Model):
    address = models.CharField(max_length=50)
    phone = models.PositiveIntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True)

# class Developer(models.Model):
#     f_name = models.CharField(max_length=50)
#     l_name = models.CharField(max_length=50)
#     email  = models.EmailField(unique=True)
