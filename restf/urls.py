from django.conf.urls import url
from django.urls import path, include
from rest_framework import routers
from restf import views
from restf.views import ProfileView

# from restf.views import UsercreateView

routers = routers.DefaultRouter()
routers.register(r'developer',views.DeveloperViewSet)
routers.register(r'', views.ProfileView)

urlpatterns = [
    # path  ('create/new', UsercreateView.as_view() , name = 'create_users'),
    # path ('',ProfileView.as_view()),
    path('', include(routers.urls)),
]
