from django.contrib.auth.models import User

from restf.models import Profile
from rest_framework import serializers, request


# class DeveloperSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Developer
#         fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email')


class ProfileReadSerializer(serializers.ModelSerializer):
    user = UserSerializer(data={'user': User})
    user_id = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all(), source='user', write_only=True, )

    class Meta:
        model = Profile
        fields = ('user', 'address', 'phone', 'user_id')


class ProfileWriteSerializer(serializers.ModelSerializer):
    user = UserSerializer(data={'user': User})
    user_id = serializers.PrimaryKeyRelatedField
    

    class Meta:
        model = Profile
        fields = ('user', 'address', 'phone', 'user_id')

    # def create(self, validated_data):
    # def create(self, validated_data):
    #     user_data = validated_data.pop('user')
    #     user = User.objects.create(**validated_data)
    #     Profile.objects.create(user=user, **user_data)
    #     return user
